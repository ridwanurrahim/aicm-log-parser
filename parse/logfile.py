import math
from export.csvformat import write_csv
from read.logfile import read_file, read_file_name


def parse_format(log_data):
    rows = []

    for index, line in enumerate(log_data):
        position = index % 6
        number = math.floor(index / 6)
        if position == 0:
            first = line.index('[')
            last = line.index(']')
            date = line[first + 1:last]
            rows.append({'date': date})
        elif position == 1:
            key, value = line.split('=>')
            name = value.replace(',', '').replace("'", '').strip()
            rows[number]['name'] = name
        elif position == 2:
            key, value = line.split('=>')
            email = value.replace(',', '').replace(' ', '').replace("'", '').replace("\n", '')
            rows[number]['email'] = email
        elif position == 3:
            key, value = line.split('=>')
            phone = value.replace(',', '').replace(' ', '').replace("'", '').replace("\n", '')
            rows[number]['phone'] = phone
        elif position == 4:
            key, value = line.split('=>')
            message = value.strip()
            message = message[1:-2]
            rows[number]['message'] = message
        elif position == 5:
            continue
    return rows


def grep_info(log_data):
    data = []
    data_index = None
    for index, line in enumerate(log_data):
        if 'local.INFO:' in line:
            data.append(line)
            data_index = index
        elif data_index is not None and data_index < index <= data_index + 5:
            data.append(line)
        else:
            continue
    return data


def parse_and_convert():
    log_data = read_file()
    remove_error = grep_info(log_data)
    rows = parse_format(remove_error)
    log_data.close()
    header = ["Date", "Name", "Email", "Phone", "Message"]
    output_file_name = read_file_name()
    write_csv(output_file_name, header, rows)
