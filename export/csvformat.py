import csv


def write_csv(file_name, header, rows):
    file = ''
    count = 0
    while True:
        try:
            file = open('output_' + file_name + '_' + str(count) + '.csv', 'w', newline='', encoding="utf-8")
        except Exception as e:
            print(e)
            count += 1
        else:
            writer = csv.writer(file)
            writer.writerow(header)

            for person in rows:
                item = [person['date'], person['name'], person['email'], person['phone'], person['message']]
                writer.writerow(item)

            file.close()
            break
