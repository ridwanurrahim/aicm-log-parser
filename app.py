import tkinter as tk
from tkinter import messagebox
from functools import partial

from parse.logfile import parse_and_convert
from read.logfile import get_log


def exit_application(root):
    message_box = tk.messagebox.askquestion('Exit Application', 'Are you sure you want to exit the application',
                                            icon='warning')
    if message_box == 'yes':
        root.destroy()


def main():
    root = tk.Tk()
    root.title("Converter")
    root.resizable(0, 0)

    canvas = tk.Canvas(root, width=350, height=300, bg='lightsteelblue2', relief='raised')
    canvas.pack()

    label1 = tk.Label(root, text='AICM Log Conversion Tool', bg='lightsteelblue2')
    label1.config(font=('helvetica', 20))
    canvas.create_window(180, 60, window=label1)

    browse_button_log = tk.Button(text="    Import Log File   ", command=get_log, bg='green', fg='white',
                                  font=('helvetica', 12, 'bold'))
    canvas.create_window(180, 130, window=browse_button_log)

    convert_csv = tk.Button(text='   Convert to CSV   ', command=parse_and_convert, bg='green', fg='white',
                            font=('helvetica', 12, 'bold'))
    canvas.create_window(180, 180, window=convert_csv)

    exit_button = tk.Button(root, text='   Exit Application   ', command=partial(exit_application, root),
                            bg='brown', fg='white', font=('helvetica', 12, 'bold'))
    canvas.create_window(180, 230, window=exit_button)

    text = tk.Label(root, text="Developed By: Ridwanur Rahim", bg='lightsteelblue2', fg='white',
                    font=('helvetica', 6, 'italic'))
    canvas.create_window(58, 293, window=text)
    root.mainloop()


if __name__ == "__main__":
    main()
