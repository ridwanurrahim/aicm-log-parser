from tkinter import filedialog
import os

file = ''
file_name_without_ext = ''


def get_log():
    global file
    global file_name_without_ext

    import_file_path = filedialog.askopenfilename()

    file_name = os.path.basename(import_file_path)
    file_name_without_ext = os.path.splitext(file_name)[0]

    file = open(import_file_path, 'r', encoding="utf-8")


def read_file():
    global file
    return file


def read_file_name():
    global file_name_without_ext
    return file_name_without_ext
